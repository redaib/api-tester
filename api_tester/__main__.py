from .api_test import run_test 
import typer


app = typer.Typer(help = "API tester !") 

@app.command(name = 'test') 
def test_api() : 
    try :
        run_test() 
    except Exception as err : 
        typer.secho(err, fg=typer.colors.RED)


