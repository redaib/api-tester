from distutils.log import error
import json
from loguru import logger


def get_dict_key(dict_ ) :
    try :
        return list(dict_.items())[0]
    except Exception as error_message :
        logger.error(error_message)


origin = 'state.json'
current = 'test.json'

with open(origin , 'r' ) as origin_file :
    origin_result = json.load(origin_file)['http://localhost:8000'][0]

with open(current , 'r' ) as current_file :
    current_result = json.load(current_file)['http://localhost:8000'][0]


# Check the set of keys that we comparing
same_endpoints = set(current_result.keys()) == set(origin_result.keys()) 
logger.info(f'Comparing same endpoints ? {same_endpoints }')
if not same_endpoints : 
    logger.warning("Missing endpoints will make the comparison fail !")

# Comparing the results 
is_equal = current_result == origin_result 

if is_equal : 
    logger.info(f'Current result match original API state!')
elif same_endpoints : 
    for child_endpoint in origin_result : 
        logger.debug(f'{child_endpoint} : {origin_result[child_endpoint] == current_result[child_endpoint]}') 