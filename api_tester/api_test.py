from time import perf_counter
from requests import get  , post

from dataclasses import dataclass , field 
from loguru import logger 
import json 
import yaml 
from pathlib import Path
from multiprocessing.pool import Pool , ThreadPool 
import os 

@dataclass 
class TestConfiguration : 
    request : str 
    parameters : list | dict  = field(default_factory= list )
    body : dict = field(default_factory= dict )

    def __post_init__(self , ) : 
        if '$' in self.body : 
            path = Path(self.body[1:])
            if path.exists() : 
                with path.open('r') as file : 
                    self.body  = json.load(file )

        converted_params = {} 
        for param in self.parameters : 
            param_key , param_value  = list(param.items())[0]  
            try : 
                converted_params[param_key].append(param_value ) 
            except KeyError : 
                converted_params[param_key] = [param_value] 
        
        self.parameters = converted_params 

    def execute(self , url ) : 
        match self.request : 
            case 'get' : 
                r = get(url = url , params = self.parameters )

            case 'post' :
                r = post(url = url , json = self.body ) 
            case _ :
                raise ValueError(f"{self.request} is unsupported request type !")
        return r

    @staticmethod 
    def from_yaml(configuration : str ) : 
        with open(configuration, "r") as f:
            config_file = yaml.load(f, yaml.loader.SafeLoader)
        #Looping over the target apis
        for api_url in config_file : 
            # Looping over the target root endpoints to test
            for endpoint in config_file[api_url] : 
                for child_endpoint in config_file[api_url][endpoint] : 
                    test_scenario = config_file[api_url][endpoint][child_endpoint]  
                    config_file[api_url][endpoint][child_endpoint] = TestConfiguration(**test_scenario )
        return config_file  

        
class TestRunner() : 
    def __init__(self ,tests_config : dict , save ) -> None:
        self.tests_configuration = tests_config
        self.save = save 

    def run( self , compare = False ) : 
        api_url = list(self.tests_configuration.keys())[0]
        test_targets = self.tests_configuration[api_url].items() 
        with Pool(len(test_targets) ) as endpoints_pool : 
            results = endpoints_pool.map(TestRunner.run_tests , ((api_url , root_endpoint , config )for root_endpoint , config in test_targets) )
        results = {child_endpoint : result for child_endpoint , result in zip(self.tests_configuration[api_url].keys() , results )}
        if self.save : 
            filename = 'state.json' if 'state.json' not in os.listdir() else 'test.json' 
            with open(filename , 'w') as test_result : 
                json.dump({api_url : results } , test_result ,   indent = 4)  


        


    @staticmethod 
    def run_tests(  target : tuple ) : 
        api_url , root_endpoint  , configs  = target 
        with ThreadPool(len(configs)) as p : 
            test_results = p.starmap(TestRunner.run_test , ( (f'{api_url}/{root_endpoint}/{child_endpoint}' , test_configuration ) for child_endpoint , test_configuration in configs.items() ) )

        return {child_endpoint : result for child_endpoint , result in zip(configs , test_results)}
        
    @staticmethod
    def run_test( request_url, test_configuration  ) : 
        start = perf_counter()
        r = test_configuration.execute(request_url)  
        logger.info(f'{request_url } : {r.status_code} in {perf_counter() - start :,.2f}')   
        try :
            return  json.loads(r.content)
        except :
            return {'response' : r.content.decode()}


def run_test():  
    tests_config = TestConfiguration.from_yaml('tests.yaml')
    test_runner = TestRunner( tests_config  , save = True ) 
    test_runner.run(compare = False)