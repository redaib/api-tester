# Introduction

Automate API testing from a YAML file using a similar format to a swagger. ( With plans to directly support the swagger file directly.)

```yaml 
"http://localhost:8000":
  "vessels" :
    "search" : 
        request : "get" 
        parameters: 
          - mmsi : 533190254 
          - mmsi : 503009420
        
    "search_any" : 
      request  : "get"
      parameters: 
        - query : 'SELECT * FROM vessel'
    "ship-type" : 
      request  :  "get" 
      parameters: 
        - imo : 9414864 
        - imo : 9414864 
        - imo : 9414864 
        - mmsi : 533190254 
        - mmsi : 533190254 
        - mmsi : 533190254 
```